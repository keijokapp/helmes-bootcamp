package ee.helmes.bootcamp.services;

import ee.helmes.bootcamp.models.Pizzeria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PizzeriaService {

	private static Logger logger = LoggerFactory.getLogger(PizzeriaService.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	public void addPizzeria(Pizzeria pizzeria) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(pizzeria);
		logger.info("(addPizzeria) saved: ", pizzeria);
	}

	@Transactional
	public void updatePizzeria(Pizzeria pizzeria) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(pizzeria);
		logger.info("(updatePizzeria) updated: ", pizzeria);
	}

	@Transactional
	public void deletePizzeria(Pizzeria pizzeria) {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(pizzeria);
		logger.info("(deletePizzeria) deleted: {}", pizzeria);
	}

	@Transactional
	public Pizzeria getPizzeria(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Pizzeria pizzeria = (Pizzeria) session.load(Pizzeria.class, id);
		logger.info("(getPizzeria) loaded: {}", pizzeria);
		return pizzeria;
	}

	@Transactional
	public List<Pizzeria> getPizzerias() {
		Session session = this.sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<Pizzeria> pizzeriaList = session.createCriteria(Pizzeria.class).list();
		for(Pizzeria pizzeria: pizzeriaList) {
			logger.info("(getPizzerias) list item: {}", pizzeria);
		}
		return pizzeriaList;
	}
}
