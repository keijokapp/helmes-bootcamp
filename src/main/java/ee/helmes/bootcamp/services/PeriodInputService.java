package ee.helmes.bootcamp.services;

import ee.helmes.bootcamp.models.PeriodRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Service
public class PeriodInputService {

	private static Logger logger = LoggerFactory.getLogger(PeriodInputService.class);

	/**
	 * Returns range of days forming full weeks.
	 * Defaults to last full week.
	 *
	 * @param periodRequest user input
	 * @return array containing start (inclusive) and end (exclusive) of the period
	 */
	public LocalDate[] fullWeeks(PeriodRequest periodRequest) {
		LocalDate start = periodRequest.getStartDate();
		LocalDate end = periodRequest.getEndDate();

		if(start == null && end == null) {
			LocalDate now = LocalDate.now();
			start = end = now.minus(1, ChronoUnit.WEEKS);
		} else if(start == null) {
			start = end;
		} else if(end == null) {
			end = start;
		} else if(start.isAfter(end)) {
			// start is bigger that end, swap
			LocalDate tmp = start;
			start = end;
			end = tmp;
		}


		// as noted in http://stackoverflow.com/a/28454044
		// it would not work in some countries but I don't think I should care
		start = start.with(DayOfWeek.MONDAY);
		end = end.with(DayOfWeek.SUNDAY);

		logger.debug("Period input: ({};{})", start, end);

		end = end.plus(1, ChronoUnit.DAYS); // now it is exclusive

		return new LocalDate[] { start, end };
	}

	/**
	 * Returns range of days.
	 * Defaults to last full week.
	 *
	 * @param periodRequest user input
	 * @return array containing start (inclusive) and end (exclusive) of the period
	 */
	public LocalDate[] days(PeriodRequest periodRequest) {
		LocalDate start = periodRequest.getStartDate();
		LocalDate end = periodRequest.getEndDate();

		if(start == null && end == null) {
			LocalDate now = LocalDate.now();
			start = end = now.minus(1, ChronoUnit.WEEKS);
			// as noted in http://stackoverflow.com/a/28454044
			// it would not work in some countries but I don't think I should care
			start = start.with(DayOfWeek.MONDAY);
			end = end.with(DayOfWeek.SUNDAY);
		} else if(start == null) {
			start = end;
		} else if(end == null) {
			end = start;
		} else if(start.isAfter(end)) {
			// start is bigger that end, swap
			LocalDate tmp = start;
			start = end;
			end = tmp;
		}

		logger.debug("Period input: ({};{})", start, end);

		end = end.plus(1, ChronoUnit.DAYS); // now it is exclusive

		return new LocalDate[] { start, end };
	}
}
