package ee.helmes.bootcamp.services;

import ee.helmes.bootcamp.models.Booking;
import ee.helmes.bootcamp.models.Pizzeria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Service
public class BookingService {
	private static final Logger logger = LoggerFactory.getLogger(BookingService.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	public void addBooking(Booking booking) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(booking);
	}

	@Transactional
	public void updateBooking(Booking booking) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(booking);
	}

	@Transactional
	public List<Booking> getBookings() {
		Session session = this.sessionFactory.getCurrentSession();

		@SuppressWarnings("unchecked")
		List<Booking> bookingList = session.createCriteria(Booking.class)
				.addOrder(Order.asc("fromTime"))
				.list();

		return bookingList;
	}

	@Transactional
	public List<Booking> getBookings(Pizzeria pizzeria) {
		Session session = this.sessionFactory.getCurrentSession();

		@SuppressWarnings("unchecked")
		List<Booking> bookingList = session.createCriteria(Booking.class)
				.add(Restrictions.eq("pizzeria", pizzeria))
				.addOrder(Order.asc("fromTime"))
				.list();

		return bookingList;
	}

	@Transactional
	public List<Booking> getBookings(LocalDate fromDate, LocalDate toDate) {
		Date from = Date.from(fromDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		Date to = Date.from(toDate.atStartOfDay(ZoneId.systemDefault()).toInstant());

		Session session = this.sessionFactory.getCurrentSession();

		@SuppressWarnings("unchecked")
		List<Booking> bookingList = session.createCriteria(Booking.class)
				.add(Restrictions.ge("fromTime", from))
				.add(Restrictions.lt("fromTime", to))
				.addOrder(Order.asc("fromTime"))
				.list();

		return bookingList;
	}

	@Transactional
	public List<Booking> getBookings(Pizzeria pizzeria, LocalDate fromDate, LocalDate toDate) {
		Date from = Date.from(fromDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		Date to = Date.from(toDate.atStartOfDay(ZoneId.systemDefault()).toInstant());

		Session session = this.sessionFactory.getCurrentSession();

		@SuppressWarnings("unchecked")
		List<Booking> bookingList = session.createCriteria(Booking.class)
				.add(Restrictions.eq("pizzeria", pizzeria))
				.add(Restrictions.ge("fromTime", from))
				.add(Restrictions.lt("fromTime", to))
				.list();
		return bookingList;
	}

	@Transactional
	public Booking getBooking(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Booking booking = (Booking) session.load(Booking.class, id);
		logger.info("Booking loaded successfully, Booking details=" + booking);
		return booking;
	}
}
