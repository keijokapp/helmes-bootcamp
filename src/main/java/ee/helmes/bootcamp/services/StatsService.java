package ee.helmes.bootcamp.services;

import ee.helmes.bootcamp.enums.Channel;
import ee.helmes.bootcamp.models.Booking;
import ee.helmes.bootcamp.models.Pizzeria;
import ee.helmes.bootcamp.models.PizzeriaTable;
import org.hibernate.*;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.type.IntegerType;
import org.hibernate.type.ShortType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.*;

@Service
public class StatsService {

	private static Logger logger = LoggerFactory.getLogger(StatsService.class);

	@Autowired
	private SessionFactory sessionFactory;


	@Transactional
	public Map<Pizzeria, int[]> daily(LocalDate start, LocalDate end) {
		Session session = this.sessionFactory.getCurrentSession();

		logger.debug("So far... {} {}", start.toString(), end.toString());

		List<Object[]> rows = session.createSQLQuery(
				"SELECT pizzeria.*, WEEKDAY(from_time) AS day, COUNT(*) AS count " +
						"FROM booking " +
						"INNER JOIN  pizzeria ON pizzeria.id = booking.pizzeria " +
						"WHERE active = TRUE " +
						"AND from_time >= :start_date AND from_time < :end_date " +
						"GROUP BY pizzeria.id, day ORDER BY pizzeria.id, day")
				.addEntity(Pizzeria.class)
				.addScalar("day", ShortType.INSTANCE)
				.addScalar("count", IntegerType.INSTANCE)
				.setParameter("start_date", start.toString())
				.setParameter("end_date", end.toString())
				.list();

		Map<Pizzeria, int[]> result = new LinkedHashMap<Pizzeria, int[]>();

		for(Object[] row: rows) {
			Pizzeria pizzeria = (Pizzeria)row[0];
			Short day = (Short)row[1];
			Integer count = (Integer)row[2];

			logger.debug("row: {} {} {}", pizzeria, day, count);

			int[] weekdays = result.get(pizzeria);
			if(weekdays == null) {
				weekdays = new int[7];
				result.put(pizzeria, weekdays);
			}

			weekdays[day] = count;
		}

		return result;
	}


	@Transactional
	public Map<Pizzeria, int[]> hourly(LocalDate start, LocalDate end) {
		Session session = this.sessionFactory.getCurrentSession();

		logger.debug("So far... {} {}", start.toString(), end.toString());

		@SuppressWarnings("unchecked")
		List<Object[]> rows = session.createSQLQuery(
				"SELECT pizzeria.*, FLOOR((HOUR(from_time)-10) / 2) AS hour, COUNT(*) AS count " +
						"FROM booking " +
						"INNER JOIN  pizzeria ON pizzeria.id = booking.pizzeria " +
						"WHERE active = TRUE " +
						"AND from_time >= :start_date AND from_time < :end_date " +
						"GROUP BY pizzeria.id, hour ORDER BY pizzeria.id, hour")
				.addEntity(Pizzeria.class)
				.addScalar("hour", ShortType.INSTANCE)
				.addScalar("count", IntegerType.INSTANCE)
				.setParameter("start_date", start.toString())
				.setParameter("end_date", end.toString())
				.list();

		Map<Pizzeria, int[]> result = new LinkedHashMap<Pizzeria, int[]>();

		for(Object[] row: rows) {
			Pizzeria pizzeria = (Pizzeria)row[0];
			Short hour = (Short)row[1];
			Integer count = (Integer)row[2];

			logger.debug("row: {} {} {}", pizzeria, hour, count);

			if(hour < 0 || hour > 7) {
				logger.error("Booking time out of range: {}", hour);
				continue;
			}

			int[] hours = result.get(pizzeria);
			if(hours == null) {
				hours = new int[7];
				result.put(pizzeria, hours);
			}

			hours[hour] = count;
		}

		return result;
	}

	@Transactional
	public Map<Pizzeria, int[]> channels(LocalDate start, LocalDate end) {
		Session session = this.sessionFactory.getCurrentSession();

		logger.debug("So far... {} {}", start.toString(), end.toString());

		@SuppressWarnings("unchecked")
		List<Object[]> rows = session.createSQLQuery(
				"SELECT pizzeria.*, channel, COUNT(*) AS count " +
						"FROM booking " +
						"INNER JOIN  pizzeria ON pizzeria.id = booking.pizzeria " +
						"WHERE active = TRUE " +
						"AND from_time >= :start_date AND from_time < :end_date " +
						"GROUP BY pizzeria.id, channel ORDER BY pizzeria.id, channel")
				.addEntity(Pizzeria.class)
				.addScalar("channel", ShortType.INSTANCE)
				.addScalar("count", IntegerType.INSTANCE)
				.setParameter("start_date", start.toString())
				.setParameter("end_date", end.toString())
				.list();

		Map<Pizzeria, int[]> result = new LinkedHashMap<Pizzeria, int[]>();

		int channelLength = Channel.values().length;

		logger.debug("channelLength: {}", channelLength);

		for(Object[] row: rows) {
			Pizzeria pizzeria = (Pizzeria)row[0];
			Short channel = (Short)row[1];
			Integer count = (Integer)row[2];

			logger.debug("row: {} {} {}", pizzeria, channel, count);

			// let's just hope that Channel's items are continuous and never change

			if(channel < 0 || channel >= channelLength) {
				logger.error("Booking channel out of range: {}", channel);
				continue;
			}

			int[] channels = result.get(pizzeria);
			if(channels == null) {
				channels = new int[channelLength];
				result.put(pizzeria, channels);
			}

			channels[channel] = count;
		}

		return result;
	}

}
