package ee.helmes.bootcamp.services;

import ee.helmes.bootcamp.models.Booking;
import ee.helmes.bootcamp.models.Pizzeria;
import ee.helmes.bootcamp.models.PizzeriaTable;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.*;
import org.hibernate.mapping.Array;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TableService {

	private static Logger logger = LoggerFactory.getLogger(TableService.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	public void addTable(PizzeriaTable table) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(table);
		logger.info("(addTable) saved: ", table);
	}

	@Transactional
	public void updateTable(PizzeriaTable table) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(table);
		logger.info("(updateTable) deleted: {}", table);
	}

	@Transactional
	public void deleteTable(PizzeriaTable table) {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(table);
		logger.info("(deleteTable) deleted: {}", table);
	}

	@Transactional
	@Deprecated
	public PizzeriaTable getTable(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		PizzeriaTable table = (PizzeriaTable) session.load(PizzeriaTable.class, id);
		logger.info("(getTable) loaded: {}", table);
		return table;
	}

	@Transactional
	public PizzeriaTable getTable(Pizzeria pizzeria, Short number) {
		Session session = this.sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<PizzeriaTable> tables = session.createCriteria(PizzeriaTable.class)
				.add(Restrictions.eq("pizzeria", pizzeria))
				.add(Restrictions.eq("number", number))
				.addOrder(Order.asc("number"))
				.list();
		if(tables.isEmpty()) throw new HibernateException("Object not found");
		PizzeriaTable table = tables.get(0);
		logger.debug("(getTable) loaded: {}", table);
		return table;
	}

	@Transactional
	public List<PizzeriaTable> getTables(Pizzeria pizzeria) {
		Session session = this.sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<PizzeriaTable> tables = session.createCriteria(PizzeriaTable.class)
				.add(Restrictions.eq("pizzeria", pizzeria))
				.addOrder(Order.asc("number"))
				.list();
		for(PizzeriaTable table: tables) {
			logger.info("(getTables) list item: {}", table);
		}
		return tables;
	}

	@Transactional
	public List<PizzeriaTable> getReservedTables(Pizzeria pizzeria, LocalDateTime fromTime, LocalDateTime toTime) {
		Session session = this.sessionFactory.getCurrentSession();

		// turned out it's quite difficult to achieve with ORM, so I use HQL directly
		//
		// http://stackoverflow.com/questions/2332487/hibernate-many-to-many-collection-filtering

		logger.debug("(getReservedTables) Period ({};{})", fromTime, toTime);

		@SuppressWarnings("unchecked")
		List<PizzeriaTable> tables = session.createSQLQuery(
				"SELECT `table`.*" +
						" FROM `table`" +
						" INNER JOIN `booking-table` ON `booking-table`.table = `table`.id" +
						" INNER JOIN `booking` ON `booking-table`.booking = booking.id" +
						" WHERE (booking.to_time > :fromTime AND booking.from_time < :toTime)" +
						" ORDER BY `table`.number")
				.addEntity(PizzeriaTable.class)
				.setParameter("fromTime", fromTime.toString())
				.setParameter("toTime", toTime.toString())
				.list();

		logger.info("(getReservedTables) list: {}", tables);

		for(PizzeriaTable table: tables) {
			logger.info("(getReservedTables) list item: {}", table);
		}
		return tables;
	}




}
