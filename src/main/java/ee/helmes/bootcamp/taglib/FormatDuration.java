package ee.helmes.bootcamp.taglib;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import java.io.*;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class FormatDuration extends SimpleTagSupport {

	private Date fromTime;
	private Date toTime;

	public void setFromTime(Date date) {
		this.fromTime = date;
	}

	public void setToTime(Date date) {
		this.toTime = date;
	}

	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();

		long duration = (toTime.getTime() - fromTime.getTime()) / 1000 / 60;

		out.println(duration / 60 + "h " + duration % 60 + "min");

	}
}