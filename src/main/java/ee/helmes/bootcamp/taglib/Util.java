package ee.helmes.bootcamp.taglib;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Set;

public class Util {

	private static Logger logger = LoggerFactory.getLogger(Util.class);

	public static boolean contains(Collection collection, Object item){
		return collection != null && collection.contains(item);
	}

	public static String formatLocalDateTime(LocalDateTime localDateTime, String pattern) {
		logger.info("{} {}", localDateTime, pattern);
		if(localDateTime == null) return "";
		return localDateTime.format(DateTimeFormatter.ofPattern(pattern));
	}

	public static String formatLocalDate(LocalDate localDate, String pattern) {
		if(localDate == null) return "";
		return localDate.format(DateTimeFormatter.ofPattern(pattern));
	}

	public static Object defaultValue(Object... args) {
		for(Object o: args) {
			if(o != null) return o;
		}
		return null;
	}
}
