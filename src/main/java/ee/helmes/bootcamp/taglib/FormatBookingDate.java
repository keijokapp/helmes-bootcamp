package ee.helmes.bootcamp.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import javax.swing.text.DateFormatter;
import java.io.IOException;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatBookingDate extends SimpleTagSupport {
	private static SimpleDateFormat dateFormatterFull = new SimpleDateFormat("dd.MM.yyyy HH:mm");
//	private static SimpleDateFormat dateFormatterPartial = new SimpleDateFormat("hh:ss");

	private Date date;

	public void setDate(Date date) {
		this.date = date;
	}

	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();

		if(date != null) {
			out.println(dateFormatterFull.format(date));
		} else {
			out.println("Invalid date");
		}
	}
}