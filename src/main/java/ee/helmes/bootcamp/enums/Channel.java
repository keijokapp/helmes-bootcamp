package ee.helmes.bootcamp.enums;

/**
 * Enum representing potential sources of bookings
 */
public enum Channel {
	EMAIL("EMAIL"),	PHONE("PHONE"), OTHER("OTHER"), SELFSERVICE("SELFSERVICE");


	public final String text;

	Channel(final String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}

	public String getName() {
		return name();
	}
}