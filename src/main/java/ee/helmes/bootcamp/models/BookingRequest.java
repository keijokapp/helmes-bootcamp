package ee.helmes.bootcamp.models;

import ee.helmes.bootcamp.enums.Channel;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * Confusing "database" models with "data exchange" models is
 * horrible practise in my opinion, so I create another model
 * representing requests to create or modify booking
 */
public class BookingRequest {

	private Long pizzeria;
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private LocalDateTime fromTime;
	private Double durationInHours;
	private Short quantity;
	private String name;
	private Channel channel;
	private String contact;
	private String comments;
	private Boolean active;
	private List<Short> tables;

	public Long getPizzeria() {
		return pizzeria;
	}

	public void setPizzeria(Long pizzeria) {
		this.pizzeria = pizzeria;
	}

	public LocalDateTime getFromTime() {
		return fromTime;
	}

	public void setFromTime(LocalDateTime fromTime) {
		this.fromTime = fromTime;
	}

	public Double getDurationInHours() {
		return durationInHours;
	}

	public void setDurationInHours(Double durationInHours) {
		this.durationInHours = durationInHours;
	}

	public Short getQuantity() {
		return quantity;
	}

	public void setQuantity(Short quantity) {
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public List<Short> getTables() {
		return tables;
	}

	public void setTables(List<Short> tables) {
		this.tables = tables;
	}
}
