package ee.helmes.bootcamp.models;

import ee.helmes.bootcamp.enums.Channel;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * I don't like declaring restrictions in domain model class because
 * it would hurt long-term maintainability. Imagine if I declared
 * `name` to be at least 2 characters in length and in few months
 * I want it to be at least 5 characters in length.  I cannot update
 * restriction to 5 because of old objects and leaving it to 2
 * would not make sense in case of new objects.
 *
 * Instead, validation should be done in a userland code (controller, service, etc.).
 * In that case I could change restrictions in userland code so it
 * will not try to create new instances with invalid properties.
 *
 * Data type restrictions are exceptions, of course.
 */


@Entity
@Table(name="booking")
public class Booking implements Serializable {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name="pizzeria")
	private Pizzeria pizzeria;

	// well I'd like to use LocalDateTime here but apparently Hibernate does not support it out of the box
	@Column(name = "from_time")
	private Date fromTime;

	@Column(name = "to_time")
	private Date toTime;

	@Column
	private Short quantity;

	@Column
	private String name;

	@Column
	private String contact;

	@Column
	private String comments;

	@Column
	private Boolean active;

	@Column
	private Channel channel;

	@ManyToMany(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@JoinTable(joinColumns=@JoinColumn(name="booking"), inverseJoinColumns=@JoinColumn(name="\"table\""))
	//@Filter(name="pizzeria", condition = "\"table\".pizzeria = \"booking\".pizzeria") // that shit didn't work
	//@WhereJoinTable(clause = "`table`.pizzeria = `booking`.pizzeria")

	// confirmed, Java/Spring/Hibernate/[gazillion other libraries/frameworks/...] is the most inflexible platform I've ever encountered.
	// Because no one EVER wants to use multicolumn keys in join, right?
	// Well, it's not Java's fault, seems more like a policy "don't care, they can write a workaround if they really need it".
	private List<PizzeriaTable> tables;

	@Column(name = "insert_time")
	private Date insertTime;

	@Column(name = "update_time")
	private Date updateTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pizzeria getPizzeria() {
		return pizzeria;
	}

	public void setPizzeria(Pizzeria pizzeria) {
		this.pizzeria = pizzeria;
	}

	public Date getFromTime() {
		return fromTime;
	}

	public void setFromTime(Date fromTime) {
		this.fromTime = fromTime;
	}

	public Date getToTime() {
		return toTime;
	}

	public void setToTime(Date toTime) {
		this.toTime = toTime;
	}

	public Short getQuantity() {
		return quantity;
	}

	public void setQuantity(Short quantity) {
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	public List<PizzeriaTable> getTables() {
		return tables;
	}

	public void setTables(List<PizzeriaTable> tables) {
		this.tables = tables;
	}

	public Date getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}
