package ee.helmes.bootcamp.models;

public class TableRequest {

	private Short number;
	private Short size;

	public Short getNumber() {
		return number;
	}

	public void setNumber(Short number) {
		this.number = number;
	}

	public Short getSize() {
		return size;
	}

	public void setSize(Short size) {
		this.size = size;
	}
}
