package ee.helmes.bootcamp.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name="\"table\"")
public class PizzeriaTable implements Serializable {

	// this field is here only for convention
	// all references should be made using *pizzeria*/*number* columns, not this one
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name="pizzeria")
	private Pizzeria pizzeria;

	@Column
	private Short number;

	@Column
	private Short size;

	@Column
	private Boolean active;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pizzeria getPizzeria() {
		return pizzeria;
	}

	public void setPizzeria(Pizzeria pizzeria) {
		this.pizzeria = pizzeria;
	}

	public Short getNumber() {
		return number;
	}

	public void setNumber(Short number) {
		this.number = number;
	}

	public Short getSize() {
		return size;
	}

	public void setSize(Short size) {
		this.size = size;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
