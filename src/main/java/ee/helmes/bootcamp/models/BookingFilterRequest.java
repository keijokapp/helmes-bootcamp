package ee.helmes.bootcamp.models;

public class BookingFilterRequest {

	private Long pizzeria;

	public Long getPizzeria() {
		return pizzeria;
	}

	public void setPizzeria(Long pizzeria) {
		this.pizzeria = pizzeria;
	}
}
