package ee.helmes.bootcamp.models;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Date;

public class PeriodRequest {

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate startDate;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate endDate;

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date testDate;

	public Date getTestDate() {
		return testDate;
	}

	public void setTestDate(@DateTimeFormat(pattern = "yyyy-MM-dd") Date testDate) {
		this.testDate = testDate;
	}
}
