package ee.helmes.bootcamp.validators;

import ee.helmes.bootcamp.enums.Channel;
import ee.helmes.bootcamp.models.BookingRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.List;

public class BookingRequestValidator implements Validator {

	private static Logger logger = LoggerFactory.getLogger(BookingRequestValidator.class);

	protected boolean requireAll = false;

	public boolean isRequireAll() {
		return requireAll;
	}

	public void setRequireAll(boolean requireAll) {
		this.requireAll = requireAll;
	}

	@Override
	public boolean supports(Class clazz) {
		return BookingRequest.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		BookingRequest bookingRequest = (BookingRequest) target;

		if(bookingRequest.getPizzeria() != null) {
			if(bookingRequest.getPizzeria() <= 0) {
				errors.rejectValue("pizzeria", "Invalid");
			}
		} else if(requireAll) {
			errors.rejectValue("pizzeria", "Missing");
		}

		if(bookingRequest.getFromTime() != null) {
		} else if(requireAll) {
			errors.rejectValue("fromTime", "Not provided");
		}

		if(bookingRequest.getDurationInHours() != null) {
			double durationInHours = bookingRequest.getDurationInHours();
			if(durationInHours < 0.1 || (durationInHours * 10) - (long) (durationInHours * 10) != 0)
				errors.rejectValue("durationInHours", "Invalid");
		} else if(requireAll) {
			errors.rejectValue("durationInHours", "Missing");
		}

		if(bookingRequest.getQuantity() != null) {
			short quantity = bookingRequest.getQuantity();
			if(quantity < 1 || quantity > 50)
				errors.rejectValue("quantity", "Invalid");
		} else if(requireAll) {
			errors.rejectValue("quantity", "Missing");
		}

		if(bookingRequest.getName() != null) {
			String name = bookingRequest.getName();
			if(name.length() < 2 || name.length() > 10)
				errors.rejectValue("name", "Invalid");
		} else if(requireAll) {
			errors.rejectValue("name", "Missing");
		}

		if(bookingRequest.getChannel() != null) {
			/*Channel channel = bookingRequest.getChannel();
			try {
				Channel.valueOf(channel);
			} catch(IllegalArgumentException e) {
				errors.rejectValue("channel", "Invalid");
			}*/
		} else if(requireAll) {
			errors.rejectValue("channel", "Missing");
		}

		if(bookingRequest.getContact() != null) {
			String contact = bookingRequest.getContact();
			if(!contact.isEmpty() && !contact.matches("[0-9]{5,10}"))
				errors.rejectValue("contact", "Invalid");
		} else if(requireAll) {
			errors.rejectValue("contact", "Missing");
		}

		if(bookingRequest.getComments() != null) {
			String comments = bookingRequest.getComments();

			if(comments.length() > 500)
				errors.rejectValue("comments", "Invalid");
		} else if(requireAll) {
			errors.rejectValue("comments", "Missing");
		}

		if(bookingRequest.getTables() != null) {
			List<Short> tables = bookingRequest.getTables();
			if(tables.isEmpty()) {
				errors.rejectValue("tables", "Invalid");
			} else {
				for(Short tableNumber: tables) {
					if(tableNumber <= 0 || tableNumber > 50)
						errors.rejectValue("tables", "Invalid");
				}
			}
		} else if(requireAll) {
			errors.rejectValue("tables", "Missing");
		}

		if(bookingRequest.getActive() == null) {
			if(requireAll)
				errors.rejectValue("active", "Missing");
		}
	}
}

