package ee.helmes.bootcamp.controllers;

import ee.helmes.bootcamp.exceptions.ValidationException;
import ee.helmes.bootcamp.models.*;
import ee.helmes.bootcamp.services.PizzeriaService;
import ee.helmes.bootcamp.services.TableService;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

@Controller
@RequestMapping("table")
public class TableController {

	private static Logger logger = LoggerFactory.getLogger(TableController.class);

	@Autowired private PizzeriaService pizzeriaService;
	@Autowired private TableService tableService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String tables(Model model) {
		model.addAttribute("pizzerias", pizzeriaService.getPizzerias());
		return "table/tables";
	}

	@RequestMapping(value = "{pizzeria}", method = RequestMethod.GET)
	public String pizzeriaTables(@PathVariable("pizzeria") Long pizzeriaId, Model model) {

		try {

			Pizzeria pizzeria = pizzeriaService.getPizzeria(pizzeriaId);

			List<PizzeriaTable> tables = tableService.getTables(pizzeria);

			List<Short> unusedNumbers = new ArrayList<Short>();
			short lastReservedNumber = 0;

			// <i>tables</i> should be sorted by numbers so single loop could be used
			for(PizzeriaTable table : tables) {
				for(int i = lastReservedNumber + 1, l = table.getNumber(); i < l; i++) {
					unusedNumbers.add((short) i);
				}
				lastReservedNumber = table.getNumber();
			}
			for(int i = lastReservedNumber + 1; i <= 50; i++) {
				unusedNumbers.add((short) i);
			}

			model.addAttribute("pizzeria", pizzeria);
			model.addAttribute("unusedNumbers", unusedNumbers);
			model.addAttribute("tables", tables);

		} catch(Exception e) {
			// assume invalid pizzeria id was provided and fail silently (log error of course)
			logger.error("(pizzeriaTables) Error: {}", e);
		}

		model.addAttribute("pizzerias", pizzeriaService.getPizzerias());
		return "table/tables";
	}

	@RequestMapping(value = "{pizzeria}", method = RequestMethod.POST)
	public String pizzeriaTablesAction(@PathVariable("pizzeria") Long pizzeriaId, TableRequest tableRequest, Model model, BindingResult errors) {
		Pizzeria pizzeria;

		try {
			pizzeria = pizzeriaService.getPizzeria(pizzeriaId);
		} catch(Exception e) {
			// assume invalid pizzeria id was provided and fail with redirect (log error of course)
			logger.error("(pizzeriaTablesAction) Error: {}", e);
			return "redirect:/table";
		}

		Short number = tableRequest.getNumber();
		Short size = tableRequest.getSize();

		// FIXME: should validation code be somewhere else?

		if(number == null || number < 1 || number > 30) {
			errors.rejectValue("number", "Invalid");
		}

		if(size == null || size < 1 || size > 50) {
			errors.rejectValue("size", "Invalid");
		}

		if(!errors.hasErrors()) {
			try {
				PizzeriaTable table = new PizzeriaTable(); // our fancy newly created table

				table.setPizzeria(pizzeria);
				table.setSize(size);
				table.setNumber(number);
				table.setActive(true);

				// this should throw on duplicate number because of database-level constraint
				tableService.addTable(table);

				// shit went ok, isn't that nice (?)
				return "redirect:/table/" + pizzeriaId;
			} catch(ConstraintViolationException e) {
				// assume duplicate table number
				logger.info("(pizzeriaTablesAction) Probably duplicate table number: {}", e);
				errors.rejectValue("number", "Duplicate");
			}
		}

		model.addAttribute("tableRequest", tableRequest);
		model.addAttribute("errors", errors);

		// error(s) occurred
		// do the usual UI stuff
		// maybe I should try more DRY
		// but encapsulating this stuff into service would be nonsense from MVC perspective
		// after all, my Ctrl, C and V keys are still functioning somehow

		List<PizzeriaTable> tables = tableService.getTables(pizzeria);

		List<Short> unusedNumbers = new ArrayList<Short>();
		short lastReservedNumber = 0;
		// <i>tables</i> should be sorted by numbers so single loop could be used
		for(PizzeriaTable table: tables) {
			for(int i = lastReservedNumber + 1, l = table.getNumber(); i < l; i++) {
				unusedNumbers.add((short)i);
			}
			lastReservedNumber = table.getNumber();
		}
		for(int i = lastReservedNumber + 1; i <= 50; i++) {
			unusedNumbers.add((short)i);
		}

		model.addAttribute("pizzeria", pizzeria);
		model.addAttribute("unusedNumbers", unusedNumbers);
		model.addAttribute("tables", tables);

		model.addAttribute("pizzerias", pizzeriaService.getPizzerias());
		return "table/tables";
	}

	@RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
	public String deleteCarAction(@PathVariable Long id) {
		PizzeriaTable table = tableService.getTable(id);
		tableService.deleteTable(table);
		return "redirect:/table/" + table.getPizzeria().getId();
	}
}
