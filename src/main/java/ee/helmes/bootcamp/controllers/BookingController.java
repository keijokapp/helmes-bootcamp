package ee.helmes.bootcamp.controllers;

import ee.helmes.bootcamp.enums.Channel;
import ee.helmes.bootcamp.models.*;
import ee.helmes.bootcamp.services.BookingService;
import ee.helmes.bootcamp.services.PeriodInputService;
import ee.helmes.bootcamp.services.PizzeriaService;
import ee.helmes.bootcamp.services.TableService;
import ee.helmes.bootcamp.util.Util;
import ee.helmes.bootcamp.validators.BookingRequestValidator;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Controller
@RequestMapping("/booking")
public class BookingController {

	private static Logger logger = LoggerFactory.getLogger(BookingController.class);

	@Autowired private BookingService bookingService;
	@Autowired private PizzeriaService pizzeriaService;
	@Autowired private TableService tableService;
	@Autowired private PeriodInputService periodInputService;


	@RequestMapping(value = "", method = RequestMethod.GET)
	public String bookings(BookingFilterRequest bookingFilterRequest,
						   PeriodRequest periodRequest,
						   Model model,
						   BindingResult bindingResult) {

		if(periodRequest.getStartDate() == null && periodRequest.getEndDate() == null) {

			// set default period to today (startDate will be set by service to the same value)
			// otherwise it will be defaulted to last full week
			periodRequest.setEndDate(LocalDate.now());

		}

		LocalDate[] period = periodInputService.days(periodRequest);

		LocalDate start = period[0]; // inclusive
		LocalDate end = period[1]; // exclusive

		periodRequest.setStartDate(start);
		periodRequest.setEndDate(end.minus(1, ChronoUnit.DAYS));

		List<Booking> bookingList;
		if(bookingFilterRequest.getPizzeria() != null) {
			Pizzeria pizzeria = pizzeriaService.getPizzeria(bookingFilterRequest.getPizzeria());
			bookingList = bookingService.getBookings(pizzeria, start, end);
		} else {
			bookingList = bookingService.getBookings(start, end);
		}

		// input models
		model.addAttribute("bookingFilterRequest", bookingFilterRequest);
		model.addAttribute("periodRequest", periodRequest);

		model.addAttribute("bookings", bookingList);
		model.addAttribute("pizzerias", pizzeriaService.getPizzerias());
		return "booking/bookings";
	}

	@RequestMapping(value = "create", method = RequestMethod.GET)
	public String createBooking(Model outputModel) {

		BookingRequest bookingRequest = new BookingRequest();
		bookingRequest.setFromTime(LocalDateTime.now());
		bookingRequest.setDurationInHours(.5);
		bookingRequest.setQuantity((short)2);

		outputModel.addAttribute("newBooking", true);
		outputModel.addAttribute("bookingRequest", bookingRequest);
		outputModel.addAttribute("pizzerias", pizzeriaService.getPizzerias());
		return "booking/editBooking";
	}

	@RequestMapping(value = "create", method = RequestMethod.POST)
	public String createBookingAction(BookingRequest bookingRequest, Model outputModel, BindingResult errors) {

		BookingRequestValidator bookingRequestValidator = new BookingRequestValidator();
		bookingRequestValidator.setRequireAll(true);

		bookingRequest.setActive(true); // set this manually to avoid validation error

		bookingRequestValidator.validate(bookingRequest, errors);

		// At this point, we should write-lock the whole god damn database to avoid data loss
		// and false positive validation results but I'm not gonna do this because huge amount
		// of Enterprise Devs™ doesn't seem to give a fsck about [high] concurrency - why should
		// I be better?
		//
		// http://www.anyware.co.uk/2005/2012/11/12/the-false-optimism-of-gorm-and-hibernate/

		Pizzeria pizzeria = null;
		try {
			Long pizzeriaId = bookingRequest.getPizzeria();
			pizzeria = pizzeriaService.getPizzeria(pizzeriaId);
		} catch(Exception e) {
			errors.rejectValue("pizzeria", "Invalid");
		}

		LocalDateTime fromTime = bookingRequest.getFromTime();
		if(!fromTime.isAfter(LocalDateTime.now())) {
			errors.rejectValue("fromTime", "Time is in the past");
		}

		List<PizzeriaTable> tables = new ArrayList<PizzeriaTable>();
		if(!errors.hasErrors()) {

			// we do not check whether tables are already reserved or not
			// REST service should mark reserved tables anyway
			// however I will provide "warning" mechanism to detect these problems
			// so collisions could be manually resolved

			for(Short tableNumber: bookingRequest.getTables()) {
				try {
					PizzeriaTable table = tableService.getTable(pizzeria, tableNumber);
					tables.add(table);
				} catch(HibernateException e) {
					errors.rejectValue("tables", "Invalid");
				}
			}
		}

		if(!errors.hasErrors()) {
			Booking booking = new Booking(); // our fancy newly created booking object

			booking.setPizzeria(pizzeria);

			Date plainOldDate = Util.localDateTimeToDate(fromTime);
			booking.setFromTime(plainOldDate);

			Double durationInHours = bookingRequest.getDurationInHours();
			// simple addition should not cause any problems, right? (https://youtu.be/-5wpm-gesOY)
			Date toTime = new Date(plainOldDate.getTime() + (long)(durationInHours * 3600 * 1000));
			booking.setToTime(toTime);

			Short quantity = bookingRequest.getQuantity();
			booking.setQuantity(quantity);

			String name = bookingRequest.getName();
			booking.setName(name);

			Channel channel = bookingRequest.getChannel();
			booking.setChannel(channel);

			String contact = bookingRequest.getContact();
			booking.setContact(contact);

			String comments = bookingRequest.getComments();
			booking.setComments(comments);

			booking.setTables(tables);

			booking.setActive(true);

			booking.setInsertTime(new Date());

			logger.info("(createBookingAction) No errors (hopefully)");
			bookingService.addBooking(booking);
			return "redirect:/booking";
		} else {
			logger.info("(createBookingAction) Errors: {}", errors);
			outputModel.addAttribute("bookingRequest", bookingRequest);
			outputModel.addAttribute("pizzerias", pizzeriaService.getPizzerias());
			outputModel.addAttribute("errors", errors);
			return "booking/editBooking";
		}
	}

	@RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
	public String editBooking(@PathVariable Long id, Model outputModel) {
		Booking booking = bookingService.getBooking(id);

		BookingRequest bookingRequest = new BookingRequest();
		bookingRequest.setPizzeria(booking.getPizzeria().getId());
		bookingRequest.setFromTime(Util.dateToLocalDateTime(booking.getFromTime()));

		long duration = booking.getToTime().getTime() - booking.getFromTime().getTime();
		bookingRequest.setDurationInHours((double)duration / 1000 / 3600);
		bookingRequest.setQuantity(booking.getQuantity());
		bookingRequest.setName(booking.getName());
		bookingRequest.setChannel(booking.getChannel());
		bookingRequest.setContact(booking.getContact());
		bookingRequest.setComments(booking.getComments());
		bookingRequest.setActive(booking.getActive());

		List<PizzeriaTable> tables = booking.getTables();
		List<Short> tableIds = new ArrayList<Short>();
		for(PizzeriaTable table: tables) {
			tableIds.add(table.getNumber());
		}
		bookingRequest.setTables(tableIds);

		outputModel.addAttribute("newBooking", false); // just to be clear
		outputModel.addAttribute("bookingRequest", bookingRequest);
		outputModel.addAttribute("pizzerias", pizzeriaService.getPizzerias());
		outputModel.addAttribute("reservedTables", tableService.getReservedTables(booking.getPizzeria(),
				Util.dateToLocalDateTime(booking.getFromTime()),
				Util.dateToLocalDateTime(booking.getToTime())));
		outputModel.addAttribute("tables", tableService.getTables(booking.getPizzeria()));
		return "booking/editBooking";
	}

	@RequestMapping(value = "edit/{id}", method = RequestMethod.POST)
	public String editBookingAction(@PathVariable Long id, BookingRequest bookingRequest, Model outputModel, BindingResult errors) {

		BookingRequestValidator bookingRequestValidator = new BookingRequestValidator();
		bookingRequestValidator.setRequireAll(false);

		bookingRequestValidator.validate(bookingRequest, errors);

		Booking booking = bookingService.getBooking(id);

		Pizzeria pizzeria = null;
		if(bookingRequest.getPizzeria() != null) {
			try {
				Long pizzeriaId = bookingRequest.getPizzeria();
				pizzeria = pizzeriaService.getPizzeria(pizzeriaId);
				booking.setPizzeria(pizzeria);
			} catch(Exception e) {
				errors.rejectValue("pizzeria", "Invalid");
			}
		}

		if(bookingRequest.getFromTime() != null) {
			LocalDateTime fromTime = bookingRequest.getFromTime();
			booking.setFromTime(Util.localDateTimeToDate(fromTime));
		}

		if(bookingRequest.getDurationInHours() != null) {
			Double durationInHours = bookingRequest.getDurationInHours();
			Date toTime = new Date(booking.getFromTime().getTime() + (long)(durationInHours * 3600 * 1000));
			booking.setToTime(toTime);
		}

		if(bookingRequest.getQuantity() != null) {
			Short quantity = bookingRequest.getQuantity();
			booking.setQuantity(quantity);
		}

		if(bookingRequest.getName() != null) {
			String name = bookingRequest.getName();
			booking.setName(name);
		}

		if(booking.getChannel() != null) {
			Channel channel = bookingRequest.getChannel();
			booking.setChannel(channel);
		}

		if(booking.getContact() != null) {
			String contact = bookingRequest.getContact();
			booking.setContact(contact);
		}

		if(booking.getComments() != null) {
			String comments = bookingRequest.getComments();
			booking.setComments(comments);
		}

		if(bookingRequest.getTables() != null) {
			List<PizzeriaTable> tables = new ArrayList<PizzeriaTable>();

			for(Short tableNumber: bookingRequest.getTables()) {
				try {
					PizzeriaTable table = tableService.getTable(pizzeria, tableNumber);
					tables.add(table);
				} catch(HibernateException e) {
					errors.rejectValue("tables", "Invalid");
				}
			}

			booking.setTables(tables);
		}

		if(bookingRequest.getActive() != null) {
			Boolean active = bookingRequest.getActive();
			booking.setActive(active);
		}

		if(!errors.hasErrors()) {

			booking.setUpdateTime(new Date());

			logger.info("(createBookingAction) No errors (hopefully)");
			bookingService.updateBooking(booking);
			return "redirect:/booking";
		} else {
			logger.info("(editBookingAction) Errors: {}", errors);
			outputModel.addAttribute("newBooking", false); // just to be clear
			outputModel.addAttribute("bookingRequest", bookingRequest);
			outputModel.addAttribute("pizzerias", pizzeriaService.getPizzerias());
			outputModel.addAttribute("tables", tableService.getTables(booking.getPizzeria()));
			outputModel.addAttribute("errors", errors);
			return "booking/editBooking";
		}
	}

	@RequestMapping(value = "rest/tables", method = RequestMethod.GET, produces = "text/json")
	public @ResponseBody String getTables(@RequestParam("pizzeria") Long pizzeriaId,
										  @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm") LocalDateTime fromTime,
										  @RequestParam Double durationInHours) {

		Pizzeria pizzeria = pizzeriaService.getPizzeria(pizzeriaId);

		List<PizzeriaTable> tables = tableService.getTables(pizzeria);
		List<PizzeriaTable> reservedTables = tableService.getReservedTables(pizzeria,fromTime, fromTime.plus((long)(durationInHours * 60), ChronoUnit.MINUTES));

		// this is fairly simple case and I'm avoiding extra (JSON) libraries

		List<String> jsonTables = new ArrayList<String>();
		for(PizzeriaTable table: tables) {
			jsonTables.add("{ \"number\": " + table.getNumber() + ", \"size\": " + table.getSize() + " }");
		}

		List<String> jsonReservedTables = new ArrayList<String>();
		for(PizzeriaTable table: reservedTables) {
			jsonReservedTables.add(table.getNumber().toString());
		}

		return "{ \"tables\": [ " + String.join(", ", jsonTables) + " ], \"reserved\": [ " + String.join(", ", jsonReservedTables) + " ] }";
	}

}
