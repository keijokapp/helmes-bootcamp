package ee.helmes.bootcamp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {

	@RequestMapping("/")
	public String main() {
		return "redirect:/booking";
	}

	@RequestMapping(value = "contact", method = RequestMethod.GET)
	public String contract() {
		return "contact";
	}

}
