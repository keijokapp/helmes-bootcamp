package ee.helmes.bootcamp.controllers;

import ee.helmes.bootcamp.models.Pizzeria;
import ee.helmes.bootcamp.services.PizzeriaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class PizzeriaController {

	private static Logger logger = LoggerFactory.getLogger(PizzeriaController.class);

	@Autowired
	private PizzeriaService pizzeriaService;

	@RequestMapping(value = "pizzerias", method = RequestMethod.GET)
	public String pizzerias(Model model) {
		List<Pizzeria> pizzeriaList = pizzeriaService.getPizzerias();
		model.addAttribute("pizzerias", pizzeriaList);
		return "pizzeria/pizzerias";
	}

	@RequestMapping(value = "pizzeria/edit/{id}", method = RequestMethod.GET)
	public String editPizzeria(@PathVariable Long id, Model model) {
		Pizzeria pizzeria = pizzeriaService.getPizzeria(id);
		model.addAttribute("pizzeria", pizzeria);
		return "pizzeria/editPizzeria";
	}

	@RequestMapping(value = "pizzeria/edit/{id}", method = RequestMethod.POST)
	public String editPizzeriaAction(Pizzeria pizzeria) {
		logger.info("{}", pizzeria.getAddress());
		pizzeriaService.updatePizzeria(pizzeria);
		return "redirect:/pizzerias";
	}

	@RequestMapping(value = "pizzeria/create", method = RequestMethod.GET)
	public String createPizzeria(Model model) {
		model.addAttribute("pizzeria", new Pizzeria());
		return "pizzeria/newPizzeria";
	}

	@RequestMapping(value = "pizzeria/create", method = RequestMethod.POST)
	public String createPizzeriaAction(Pizzeria pizzeria) {
		pizzeriaService.addPizzeria(pizzeria);
		return "redirect:/pizzerias";
	}

	@RequestMapping(value = "pizzeria/delete/{id}", method = RequestMethod.GET)
	public String deletePizzeria(@PathVariable Long id, Model model) {
		Pizzeria pizzeria = pizzeriaService.getPizzeria(id);
		model.addAttribute("pizzeria", pizzeria);
		return "pizzeria/deletePizzeria";
	}

	@RequestMapping(value = "pizzeria/delete/{id}", method = RequestMethod.POST)
	public String deleteCarAction(@PathVariable Long id) {
		Pizzeria pizzeria = pizzeriaService.getPizzeria(id);
		pizzeriaService.deletePizzeria(pizzeria);
		return "redirect:/pizzerias";
	}
}
