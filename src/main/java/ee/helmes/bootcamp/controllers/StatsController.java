package ee.helmes.bootcamp.controllers;

import ee.helmes.bootcamp.models.PeriodRequest;
import ee.helmes.bootcamp.models.Pizzeria;
import ee.helmes.bootcamp.services.PeriodInputService;
import ee.helmes.bootcamp.services.StatsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Map;

@Controller
public class StatsController {

	private static Logger logger = LoggerFactory.getLogger(StatsController.class);

	@Autowired
	private StatsService statsService;

	@Autowired
	private PeriodInputService periodInputService;

	@RequestMapping("/stats")
	public String stats() {
		return "redirect:/stats/daily";
	}

	@RequestMapping(value = "/stats/daily", method = RequestMethod.GET)
	public String daily(PeriodRequest periodRequest, Model model) {

		LocalDate[] period = periodInputService.fullWeeks(periodRequest);

		LocalDate start = period[0]; // inclusive
		LocalDate end = period[1]; // exclusive

		periodRequest.setStartDate(start);
		periodRequest.setEndDate(end.minus(1, ChronoUnit.DAYS));

		Map<Pizzeria, int[]> stats = statsService.daily(start, end);

		model.addAttribute("statsRequest", periodRequest);
		model.addAttribute("stats", stats);

		return "stats/daily";
	}

	@RequestMapping(value = "/stats/hourly", method = RequestMethod.GET)
	public String hourly(PeriodRequest periodRequest, Model model) {

		LocalDate[] period = periodInputService.days(periodRequest);

		LocalDate start = period[0]; // inclusive
		LocalDate end = period[1]; // exclusive

		periodRequest.setStartDate(start);
		periodRequest.setEndDate(end.minus(1, ChronoUnit.DAYS));

		Map<Pizzeria, int[]> stats = statsService.hourly(start, end);

		model.addAttribute("statsRequest", periodRequest);
		model.addAttribute("stats", stats);

		return "stats/hourly";
	}


	@RequestMapping(value = "/stats/channels", method = RequestMethod.GET)
	public String channels(PeriodRequest periodRequest, Model model) {

		LocalDate[] period = periodInputService.days(periodRequest);

		LocalDate start = period[0]; // inclusive
		LocalDate end = period[1]; // exclusive

		periodRequest.setStartDate(start);
		periodRequest.setEndDate(end);

		Map<Pizzeria, int[]> stats = statsService.channels(start, end);

		model.addAttribute("statsRequest", periodRequest);
		model.addAttribute("stats", stats);

		return "stats/channels";
	}

}
