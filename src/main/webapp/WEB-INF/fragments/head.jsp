<head>
    <meta charset="utf-8"/>
    <title>Helmes Bootcamp</title>
    <link href="/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
    <meta name="viewport" content="width=device-width"/>
    <link href="/content/site.css" rel="stylesheet"/>
    <link href="/content/app.css" rel="stylesheet"/>

    <script src="/scripts/jquery-1.8.2.js"></script>
    <script src="/scripts/jquery.validate.js"></script>
    <!--  <script src="/scripts/site.js"></script>-->

</head>
