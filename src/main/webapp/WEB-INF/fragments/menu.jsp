<header>
    <div class="content-wrapper">
        <div class="float-left">
            <p class="site-title"><a href="/">Helmes Bootcamp sample application</a></p>
        </div>
        <div class="float-right">

            <nav>
                <ul id="menu">
                    <li><a href="/table">Tables</a></li>
                    <li><a href="/pizzerias">Pizzerias</a></li>
                    <li><a href="/booking">Bookings</a></li>
                    <li><a href="/contact">Contact</a></li>
                    <li><a href="/stats">Statistics</a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>