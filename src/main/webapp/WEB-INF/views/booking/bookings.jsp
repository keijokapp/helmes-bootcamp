<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib prefix="app" uri="/WEB-INF/taglibs/app.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="../../fragments/head.jsp"/>
<body>
<jsp:include page="../../fragments/menu.jsp"/>
<div id="body">

	<section class="content-wrapper main-content clear-fix">


		<h2>List of bookings</h2>

		<p>
			<a href="/booking/create">Create New</a>
		</p>

		<form method="get">
			<div>
				<c:forEach items="${pizzerias}" var="pizzeria">
					<label class="pizzeria-selector">
						<input type="radio"
							   name="pizzeria"
							   onchange="this.form.submit()"
							   value="${pizzeria.id}"
							   ${bookingFilterRequest.pizzeria == pizzeria.id ? " checked=\"checked\"" : ""} />
						<div class="pizzeria-container pizzeria-container-small">
							<div class="pizzeria-content-container">
								<div class="pizzeria-name">${fn:escapeXml(pizzeria.name)}</div>
								<div class="pizzeria-address">${fn:escapeXml(pizzeria.address)}</div>
							</div>
						</div>
					</label>
				</c:forEach>
				<label class="pizzeria-selector">
					<input type="radio"
						   name="pizzeria"
						   onchange="this.form.submit()"
						   value="" />
					<div class="pizzeria-container pizzeria-container-small">
						<div class="pizzeria-content-container">
							<span style="text-decoration: underline">Clear</span>
						</div>
					</div>
				</label>
			</div>
			<div>
				<label>
					From:
					<input type="date" name="startDate" value="${fn:escapeXml(periodRequest.startDate)}" />
				</label>
				<label>
					To:
					<input type="date" name="endDate" value="${fn:escapeXml(periodRequest.endDate)}" />
				</label>
			</div>
			<div>
				<input type="submit" value="Filter"/>
			</div>
		</form>

		<table class="" style="width: 100%">
			<tr>
				<th>
					Insert Time
				</th>
				<th>
					Pizzeria
				</th>
				<th>
					Time And Duration
				</th>
				<th>
					Customer
				</th>
				<th>
					Comments
				</th>
				<th></th>
			</tr>

			<c:forEach items="${bookings}" var="booking">
				<tr class="${ booking.active ?  (booking.updateTime != null ? "booking-changed" : "") : "booking-inactive" }">
					<td>
						<p><fmt:formatDate value="${booking.insertTime}" pattern="dd.MM.yyyy HH:mm" /></p>
						<c:if test="${not empty booking.updateTime}">
							<p style="font-size: 0.8em;"> Updated at <br/> <fmt:formatDate value="${booking.updateTime}" pattern="dd.MM.yyyy HH:mm" /></p>
						</c:if>
					</td>
					<td>${fn:escapeXml(booking.pizzeria.name)}</td>
					<td>
						<p><app:format-booking-date date="${booking.fromTime}"/></p>
						<p><app:format-duration fromTime="${booking.fromTime}" toTime="${booking.toTime}" /></p>
					</td>
					<td>
						<p>${fn:escapeXml(booking.name)}</p>
						<c:if test="${not empty booking.contact}">
							<p>${fn:escapeXml(booking.contact)}</p>
						</c:if>
					</td>
					<td>${fn:escapeXml(booking.comments)}<c:if test="${empty booking.comments}"><i>none</i></c:if></td>
					<td>
						<a href="/booking/edit/${booking.id}">Edit</a> |
						<a href="/booking/delete/${booking.id}">Delete</a>
					</td>
				</tr>
			</c:forEach>
			<c:if test="${empty bookings}">
				<tr>
					<td colspan="8" style="text-align: center; padding: 8px"><i>no bookings displayed</i></td>
				</tr>
			</c:if>
		</table>

	</section>
</div>
<jsp:include page="../../fragments/footer.jsp"/>

</body>
</html>
