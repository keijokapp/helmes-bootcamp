<%@ page contentType="text/html; charset=UTF-8" %>


<%@ taglib prefix="app" uri="/WEB-INF/taglibs/app.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="../../fragments/head.jsp"/>
<body>
<jsp:include page="../../fragments/menu.jsp"/>
<div id="body">

	<section class="content-wrapper main-content clear-fix">


		<h2>Details</h2>

		<fieldset>
			<legend>DbBooking</legend>
			<div class="display-field">
				<span style="color: #aaa; font-size: 1.2em; font-weight: bold;">${booking.active ? "ACTIVE" : "INACTIVE"}</span>
			</div>
			<div class="display-field">
				Channel: ${booking.channel}
			</div>
			<div class="display-field">
				Time: <app:format-booking-date date="${booking.fromTime}" />
			</div>
			<div class="display-field">
				Duration: <app:format-duration duration="${booking.duration}" />
			</div>
			<div class="display-label">
				Customer name: ${booking.name}
			</div>
			<c:if test="${customer.contact}">
				<div class="display-label">
					Customer name: ${booking.name}
				</div>
			</c:if>
			<div class="display-label">
				Booking comments:
			</div>
            <div class="display-field">
				${booking.comments != null ? booking.comments : "<i>none</i>"}
            </div>
		</fieldset>
		<p>
			<a href="/booking/edit/${booking.id}">Edit</a> |
			<a href="/bookings">Back endDate List</a>
		</p>

	</section>
</div>
<jsp:include page="../../fragments/footer.jsp"/>

</body>
</html>
