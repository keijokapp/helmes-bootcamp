<%@ page import="ee.helmes.bootcamp.enums.Channel" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib prefix="app" uri="/WEB-INF/taglibs/app.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="../../fragments/head.jsp"/>
<body>
<jsp:include page="../../fragments/menu.jsp"/>
<div id="body">
	<section class="content-wrapper main-content clear-fix">

		<c:if test="${newBooking}">
			<h2>Create</h2>
		</c:if>
		<c:if test="${!newBooking}">
			<h2>Edit</h2>
		</c:if>

		<c:if test="${errors != null}"> <!-- TODO: remove -->
			<div class="error">
				${errors}
			</div>
		</c:if>

		<form method="post" class="bookingForm">
			<fieldset>
				<legend>DbBooking</legend>

				<div>
					<div style="display: inline-block; ${errors.hasFieldErrors("pizzeria") ? "outline: orangered solid 2px" : ""}">
						<c:forEach items="${pizzerias}" var="pizzeria">
							<label class="pizzeria-selector">
								<input type="radio"
									   name="pizzeria"
									   value="${pizzeria.id}"
									${bookingRequest.pizzeria == pizzeria.id ? " checked=\"checked\"" : ""} />
								<div class="pizzeria-container pizzeria-container-small">
									<div class="pizzeria-content-container">
										<div class="pizzeria-name">${fn:escapeXml(pizzeria.name)}</div>
										<div class="pizzeria-address">${fn:escapeXml(pizzeria.address)}</div>
									</div>
								</div>
							</label>
						</c:forEach>
					</div>
				</div>

				<div>
					<label>
						Date and time:
						<input type="datetime-local" name="fromTime"
							   value="${app:formatLocalDateTime(bookingRequest.fromTime, "yyyy-MM-dd'T'HH:mm")}"
						${errors.hasFieldErrors("fromTime") ? " class=\"error\"" : ""} />
					</label>
				</div>
				<div>
					<label>
						Duration (h):
						<input type="text" name="durationInHours" style="width: 80px"
							   value="${fn:escapeXml(bookingRequest.durationInHours)}"
						${errors.hasFieldErrors("durationInHours") ? " class=\"error\"" : ""} />
					</label>
				</div>
				<div>
					<label>
						Seats (1-50):
						<input type="number" name="quantity" style="width: 80px"
							   value="${fn:escapeXml(bookingRequest.quantity)}"
						${errors.hasFieldErrors("quantity") ? " class=\"error\"" : ""} />
					</label>
				</div>
				<div>
					<label>
						Tables:
						<select name="tables" multiple="multiple" style="vertical-align: middle"
						${errors.hasFieldErrors("tables") ? " class=\"error\"" : ""}>
							<c:forEach items="${tables}" var="table">
								<option value="${table.number}" ${app:contains(bookingRequest.tables, table.number) ? "selected=\"selected\"" : ""}>No. ${table.number} - Size: ${table.size}</option>
							</c:forEach>
						</select>
					</label>
				</div>
				<div>
					<label>
						Name:
						<input type="text" name="name"
							   value="${fn:escapeXml(bookingRequest.name)}"
						${errors.hasFieldErrors("name") ? " class=\"error\"" : ""} />
					</label>
				</div>
				<div>
					<label>
						Phone:
						<input type="text" name="contact"
							   value="${fn:escapeXml(bookingRequest.contact)}"
						${errors.hasFieldErrors("contact") ? " class=\"error\"" : ""}/>
					</label>
				</div>
				<div>
					<label for="comments">Comments: </label>
				</div>
				<div>
					<textarea id="comments" name="comments"
					${errors.hasFieldErrors("comments") ? " class=\"error\"" : ""}>${fn:escapeXml(bookingRequest.comments)}</textarea>
				</div>
				<c:if test="${!newBooking}">
					<div>
						<label>
							Active:
							<input type="checkbox" name="active" value="1"
								   ${bookingRequest.active ? "checked=\"checked\"" : ""}
							${errors.hasFieldErrors("active") ? " class=\"error\"" : ""}/>
							<input type="hidden" name="active" value="0" />
						</label>
					</div>
				</c:if>
				<div>
					<label>
						Source:
						<select name="channel"
						${errors.hasFieldErrors("channel") ? " class=\"error\"" : ""}>
							<option value="">--</option>
							<c:forEach items="<%=Channel.values()%>" var="channel">
								<option value="${fn:escapeXml(channel)}"
									${bookingRequest.channel == channel.name ? "selected=\"selected\"" : ""}>${fn:escapeXml(channel)}</option>
							</c:forEach>
						</select>
					</label>
				</div>

				<p>
					<input type="submit" value="Save"/>
				</p>
			</fieldset>
		</form>
		<div>
			<a href="/bookings">Back endDate List</a>
		</div>


	</section>
</div>
<jsp:include page="../../fragments/footer.jsp"/>

	<script src="/scripts/table-loader.js"></script>
</body>
</html>
