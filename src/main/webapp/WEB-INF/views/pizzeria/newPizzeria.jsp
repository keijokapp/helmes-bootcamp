<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="../../fragments/head.jsp"/>
<body>
<jsp:include page="../../fragments/menu.jsp"/>
<div id="body">
	<section class="content-wrapper main-content clear-fix">

		<h2>Create</h2>

		<form:form modelAttribute="pizzeria">
			<fieldset>
				<legend>DbPizzeria</legend>

				<div class="editor-label">
					<label for="PizzeriaName">Pizzeria Name</label>
				</div>
				<div class="editor-field">
					<form:input path="name" id="PizzeriaName" name="name" />
				</div>
				<div class="editor-label">
					<label for="PizzeriaAddress">Pizzeria Address</label>
				</div>
				<div class="editor-field">
					<form:input path="address" id="PizzeriaAddress" name="address" />
				</div>

				<p>
					<input type="submit" value="Create"/>
				</p>
			</fieldset>
		</form:form>
		<div>
			<a href="/pizzerias">Back to List</a>
		</div>


	</section>
</div>
<jsp:include page="../../fragments/footer.jsp"/>

</body>
</html>
