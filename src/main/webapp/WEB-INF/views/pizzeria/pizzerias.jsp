<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="../../fragments/head.jsp"/>
<body>
<jsp:include page="../../fragments/menu.jsp"/>
<div id="body">

	<section class="content-wrapper main-content clear-fix">

		<h2>Pizzerias</h2>

			<c:forEach items="${pizzerias}" var="pizzeria">
				<div class="pizzeria-container">
					<div class="pizzeria-content-container">
						<div class="pizzeria-name">${pizzeria.name}</div>
					<div class="pizzeria-address">${pizzeria.address}</div>
						<div class="pizzeria-edit"><a href="/pizzeria/edit/${pizzeria.id}">Edit</a></div>
						</div>
				</div>
			</c:forEach>

			<div class="pizzeria-container">
				<div class="pizzeria-content-container">
					<div class="pizzeria-new"><a href="/pizzeria/create">Create new</a></div>
				</div>
			</div>
	</section>
</div>
<jsp:include page="../../fragments/footer.jsp"/>

</body>
</html>
