<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib prefix="app" uri="/WEB-INF/taglibs/app.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="../../fragments/head.jsp"/>
<body>
<jsp:include page="../../fragments/menu.jsp"/>
<div id="body">

	<section class="content-wrapper main-content clear-fix">


		<h2>Tables</h2>

		<form method="get">
			<div>
				<c:forEach items="${pizzerias}" var="pizzeria_loop">
					<a class="pizzeria-selector" href="/table/${pizzeria_loop.id}">
						<div class="pizzeria-container pizzeria-container-small ${pizzeria.id == pizzeria_loop.id ? "pizzeria-selected" : ""}">
							<div class="pizzeria-content-container">
								<div class="pizzeria-name">${fn:escapeXml(pizzeria_loop.name)}</div>
								<div class="pizzeria-address">${fn:escapeXml(pizzeria_loop.address)}</div>
							</div>
						</div>
					</a>
				</c:forEach>
			</div>
		</form>
		<c:if test="${not empty pizzeria}">
			<div style="border: 1px solid lightgrey">
				<h3>Create new</h3>
				<c:if test="${not empty errors}">
					<div class="error">
						${errors}
					</div>
				</c:if>
				<form method="post">
					<label>
						Number:
						<select name="number" style="width: 120px" ${errors.hasFieldErrors("number") ? " class=\"error\"" : ""}>
							<c:forEach items="${unusedNumbers}" var="tableNumber">
								<option value="${tableNumber}" ${tableRequest.number == tableNumber ? "selected=\"selected\"" : ""}>${tableNumber}</option>
							</c:forEach>
						</select>
					</label>
					<label>Seats: <input type="number" style="width: 120px" name="size" value="${tableRequest.size}" ${errors.hasFieldErrors("size") ? " class=\"error\"" : ""} /></label>
					<button type="submit">Create</button>
				</form>
			</div>
		</c:if>
		<table class="" style="width: 100%">
			<tr>
				<th>
					Pizzeria
				</th>
				<th>
					Number
				</th>
				<th>
					Seats
				</th>
				<th></th>
			</tr>

			<c:forEach items="${tables}" var="table">
				<tr class="">
					<td>${table.pizzeria.name}</td>
					<td>${table.number}</td>
					<td>${table.size}</td>
					<td>
						<a href="/table/delete/${table.id}">Delete</a>
					</td>
				</tr>
			</c:forEach>
		</table>

	</section>
</div>
<jsp:include page="../../fragments/footer.jsp"/>

</body>
</html>
