<%@ page import="ee.helmes.bootcamp.enums.Channel" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="../../fragments/head.jsp"/>
<body>
<jsp:include page="../../fragments/menu.jsp"/>
<div id="body">

	<section class="content-wrapper main-content clear-fix">


		<h2>Statistics</h2>

		<div>
			<a href="/stats/daily">Daily</a> | <a href="/stats/hourly">Hourly</a> | <a href="/stats/channels">Channels</a>
		</div>

		<form method="get">
			<div>
				<label>
					From:
					<input type="date" name="startDate" value="${fn:escapeXml(statsRequest.startDate)}" />
				</label>
				<label>
					To:
					<input type="date" name="endDate" value="${fn:escapeXml(statsRequest.endDate)}" />
				</label>
			</div>
			<div>
				<input type="submit" value="Filter"/>
			</div>
		</form>

		<table class="" style="width: 100%">
			<tr>
				<th></th>
				<c:forEach items="<%=Channel.values()%>" var="channel">
					<th>
						${fn:escapeXml(channel)}
					</th>
				</c:forEach>
			</tr>

			<c:forEach items="${stats}" var="statsEntry">
				<tr class="">
					<td>${fn:escapeXml(statsEntry.key.address)}</td>

					<c:forEach items="${statsEntry.value}" var="channelValue">
						<td>${channelValue}</td>
					</c:forEach>
				</tr>
			</c:forEach>
		</table>

	</section>
</div>
<jsp:include page="../../fragments/footer.jsp"/>

</body>
</html>
