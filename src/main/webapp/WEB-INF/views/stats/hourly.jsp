<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="../../fragments/head.jsp"/>
<body>
<jsp:include page="../../fragments/menu.jsp"/>
<div id="body">

	<section class="content-wrapper main-content clear-fix">


		<h2>Statistics</h2>

		<div>
			<a href="/stats/daily">Daily</a> | <a href="/stats/hourly">Hourly</a> | <a href="/stats/channels">Channels</a>
		</div>

		<form method="get">
			<div>
				<label>
					From:
					<input type="date" name="startDate" value="${statsRequest.startDate}" />
				</label>
				<label>
					To:
					<input type="date" name="endDate" value="${statsRequest.endDate}" />
				</label>
			</div>
			<div>
				<input type="submit" value="Filter"/>
			</div>
		</form>

		<table class="" style="width: 100%">
			<tr>
				<th></th>
				<th>
					10-12
				</th>
				<th>
					12-14
				</th>
				<th>
					14-16
				</th>
				<th>
					16-18
				</th>
				<th>
					18-20
				</th>
				<th>
					20-22
				</th>
				<th>
					22-00
				</th>
			</tr>

			<c:forEach items="${stats}" var="statsEntry">
				<tr class="">
					<td>${fn:escapeXml(statsEntry.key.address)}</td>
					<td>${statsEntry.value[0]}</td>
					<td>${statsEntry.value[1]}</td>
					<td>${statsEntry.value[2]}</td>
					<td>${statsEntry.value[3]}</td>
					<td>${statsEntry.value[4]}</td>
					<td>${statsEntry.value[5]}</td>
					<td>${statsEntry.value[6]}</td>
				</tr>
			</c:forEach>
		</table>

	</section>
</div>
<jsp:include page="../../fragments/footer.jsp"/>

</body>
</html>
