// no need for jQuery here, I can handle it myself

// I would prefer using ES6 but then I'd need to setup another repository/codebase and build system

;(function() {
	var form = document.forms[0];
	var pizzeriaElements = form.elements.pizzeria;
	var fromTimeElement = form.elements.fromTime;
	var durationElement = form.elements.durationInHours;
	var tablesElement = form.elements.tables;

	var xhr = null; // to enable aborting and avoid simultaneous requests

	function reload() {
		if(xhr != null) {
			xhr.abort();
			xhr = null;
		}

		if(pizzeriaElements.value && fromTimeElement.value && durationElement.value) {
			xhr = new XMLHttpRequest();

			xhr.open('GET', '/booking/rest/tables'
				+ '?pizzeria=' + encodeURIComponent(pizzeriaElements.value) // just in case
				+ '&fromTime=' + encodeURIComponent(fromTimeElement.value)
				+ '&durationInHours=' + encodeURIComponent(durationElement.value));

			xhr.onload = function() {
				xhr = null;

				// 200-399 should represent some kind of 'OK' status but I'm not actually sure
				if(this.status >= 200 && this.status < 400 && this.getResponseHeader('Content-Type') === 'text/json;charset=UTF-8') {
					try {
						var response = JSON.parse(this.responseText);

						// TODO: save current selection

						var options = [ ];
						for(var i = 0, l = response.tables.length; i < l; i++) {
							var number = response.tables[i].number;
							var size = response.tables[i].size;
							var reserved = response.reserved.indexOf(response.tables[i].number) !== -1;

							options.push(`<option value="${number}" ${reserved ? 'style="color: gray"' : ''}>No. ${number} - Size: ${size}</option>`);
						}
						tablesElement.innerHTML = options.join('');
					} catch(e) {
						console.error('Invalid response: %s', this.responseText);
					}
				} else {
					console.error('Unexpected response headers: Status: %d; Content-Type: %s', this.status, this.getResponseHeader('Content-Type'));
				}
			};

			xhr.send(null);
		}
	}

	// register event listeners
	for(var i = 0, l = pizzeriaElements.length; i < l; i++) {
		pizzeriaElements[i].addEventListener('change', reload);
	}
	fromTimeElement.addEventListener('change', reload);
	durationElement.addEventListener('change', reload);

	// reload(); // reload if fields are filled
})();