/**
 * Created by priit.serk on 10.03.2016.
 */
$().ready(function () {
    $(".bookingForm").validate({
        rules: {
            bookingInformation: "required"
        },
        messages: {
            bookingInformation: "Booking information is required. Field cannot be empty!"
        }
    });
});